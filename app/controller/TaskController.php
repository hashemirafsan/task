<?php 

/**
 * Add your model
 * What you need
 */

require 'model/users.php';

/**
 * 
 * It's excutable Controller
 * Create at first the dynamic
 * Database query and make instance
 * of query in here to get data
 * from database
 * 
 */

class TaskController {

	public function __construct() {
	
		$this->execute();
	}


	/**
	 * This is your executable
	 * method. you can use it 
	 * or you can make another
	 * method and add here to
	 * excute
	 * 
	 */
	public function execute()
	{
		
	}

}

?>