<?php

/**
 * Database Connection
 */

class Connection {


	public $host;
	public $user;
	public $pass;
	public $db;
	public $con;

	public function __construct()
	{
		$this->host = $_ENV['host'];
		$this->user = $_ENV['user'];
		$this->pass = $_ENV['pass'];
		$this->db = $_ENV['db'];

		$this->con = new mysqli(
			$this->host,
			$this->user,
			$this->pass,
			$this->db
		);

		if($this->con->connect_error) {
			echo 'Connection Failed!';
			die();
		}
	}

}

?>